$(document).ready(function() {

  $('.accordion1').hide();
  $('.accordion2').hide();
  $('.accordion3').hide();
  $('.accordion4').hide();
  $('.order').hide();
  $('.final').hide();

  $('#food1').click(function() {
    $('#store1').hide();
    $('#store2').hide();
    $('#store3').hide();
    $('#store4').hide();
    $('.accordion1').show();
    return false;
  })

  $('#food2').click(function() {
    $('#store1').hide();
    $('#store2').hide();
    $('#store3').hide();
    $('#store4').hide();
    $('.accordion2').show();
    return false;
  })

  $('#food3').click(function() {
    $('#store1').hide();
    $('#store2').hide();
    $('#store3').hide();
    $('#store4').hide();
    $('.accordion3').show();
    return false;
  })

  $('#food4').click(function() {
    $('#store1').hide();
    $('#store2').hide();
    $('#store3').hide();
    $('#store4').hide();
    $('.accordion4').show();
    return false;
  })

  $('#click1').click(function() {
    showOrder("ข้าวผัดกุ้ง")
    $('.accordion1').hide();
    $('.order').show();
    return false;
  })

  $('#click2').click(function() {
    showOrder("ข้าวหน้าเป็ด")
    $('.accordion1').hide();
    $('.order').show();
    return false;
  })

  $('#click3').click(function() {
    showOrder("ข้าวมันไก่ทอด")
    $('.accordion1').hide();
    $('.order').show();
    return false;
  })

  $('#click4').click(function() {
    showOrder("ข้าวแกงกะหรี่หมูทงคัตสึ")
    $('.accordion2').hide();
    $('.order').show();
    return false;
  })

  $('#click5').click(function() {
    showOrder("ข้าวแซลมอนย่างซีอิ๊ว")
    $('.accordion2').hide();
    $('.order').show();
    return false;
  })

  $('#click6').click(function() {
    showOrder("ผัดไทยกุ้งสด")
    $('.accordion1').hide();
    $('.order').show();
    return false;
  })

  $('#click7').click(function() {
    showOrder("ราดหน้าหมูหมัก")
    $('.accordion1').hide();
    $('.order').show();
    return false;
  })

  $('#click8').click(function() {
    showOrder("ส้มตำ")
    $('.accordion3').hide();
    $('.order').show();
    return false;
  })

  $('#click9').click(function() {
    showOrder("คอหมูย่าง")
    $('.accordion3').hide();
    $('.order').show();
    return false;
  })

  $('#click10').click(function() {
    showOrder("เค้กฝอยทองมะพร้าวอ่อนลาวา")
    $('.accordion4').hide();
    $('.order').show();
    return false;
  })

  $('#click11').click(function() {
    showOrder("เลมอนชีสพาย")
    $('.accordion4').hide();
    $('.order').show();
    return false;
  })

  $('#click12').click(function() {
    showOrder("คุกกี้ดับเบิ้ลดาร์คช็อกโก")
    $('.accordion4').hide();
    $('.order').show();
    return false;
  })

  $('#click13').click(function() {
    showOrder("เครปเค้กสายรุ้ง")
    $('.accordion4').hide();
    $('.order').show();
    return false;
  })

  $('#click14').click(function() {
    showOrder("เค้กส้ม")
    $('.accordion4').hide();
    $('.order').show();
    return false;
  })

  $('#click15').click(function() {
    showOrder("คุกกี้มัทฉะไวท์ช็อก")
    $('.accordion4').hide();
    $('.order').show();
    return false;
  })

  $('#click16').click(function() {
    showOrder("มาการอง")
    $('.accordion4').hide();
    $('.order').show();
    return false;
  })

  $('.order').submit(function() {
    name = $('#inputName').val();
    last = $('#inputLast').val();
    address = $('#inputAddress').val();
    code = $('#inputCode').val();
    tel = $('#inputTel').val();
    if (name != null && last != null && address != null && code != null && tel != null) {
      $('.order').hide();
      $('.final').show();
    }
    return false;
  })

  function showOrder(menu) {
    $(".your_menu").html(menu.toString())
  }
})